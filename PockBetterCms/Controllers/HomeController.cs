﻿using BetterCms.Module.Api;
using BetterCms.Module.Api.Infrastructure;
using BetterCms.Module.Api.Infrastructure.Enums;
using BetterCms.Module.Api.Operations.Pages.Contents.Content;
using BetterCms.Module.Api.Operations.Pages.Pages;
using BetterCms.Module.Api.Operations.Pages.Pages.Page.Contents.Content;
using BetterCms.Module.Api.Operations.Pages.Widgets;
using PockBetterCms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PockBetterCms.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var modelHome = new HomeViewModel();


            using (var api = ApiFactory.Create())
            {
                //var requestModel = new GetWidgetsModel();
                //requestModel.Take = 5; // Retrieves 5 items
                //requestModel.Skip = 3; // Skips 3 items

                //requestModel.Order.Add("Name"); // Sorts by name ascending
                //requestModel.Order.Add("CreatedOn", OrderDirection.Desc); // Then sorts by date descending

                //requestModel.Filter.Connector = FilterConnector.And; // Filtering connector (optional, default value = FilterConnector.And)
                //requestModel.Filter.Add("Name", "A", FilterOperation.StartsWith); // Filters by name: item should start with "A"
                //requestModel.Filter.Add("Name", "B", FilterOperation.EndsWith); // Filters by name: item should start with "B"

                //var innerFilter = new DataFilter(FilterConnector.Or); // Create inner filter
                //innerFilter.Add("CreatedBy", "Name.Surname.1");
                //innerFilter.Add("CreatedBy", "Name.Surname.2");
                //requestModel.Filter.Inner.Add(innerFilter); // Add inner filter to filters list

                //var request = new GetWidgetsRequest { Data = requestModel }; // Create request

                var requestModel = new GetPageContentModel()
                {
                    IncludeOptions = true
                };

                var request = new GetPageContentRequest()
                {
                    Data = requestModel,
                    PageContentId = new Guid("6389AFE5-5670-4F9A-B38C-A65500E4BD7A"),
                    PageId = new Guid("8A28CB67-7A80-4CEF-BB3B-A65500E434CA")
                };

                var movel = new GetPagesModel()
                {
                    FilterByCategoriesNames = new List<string>() { "Default Category Tree" }
                };
                var pages = api.Pages.Pages.Get(new GetPagesRequest() { Data = movel });

                var contents = api.Pages.Page.Content.Get(request); // Execute query
            }

            return View(modelHome);
        }
    }
}